# Copy of bashrc
cp ./.bashrc ~/.bashrc

# Copy .gitconfig to user folder
cp ./.gitconfig ~/.gitconfig

# Copy .vimrc to user folder
cp ./.vimrc ~/.vimrc

# Copy .inputrc to user folder (for case-insensitive tabbing in bash)
cp ./.inputrc ~/.inputrc

# Copy syncthing autostart
cp ./autostart/syncthing.desktop ~/.config/autostart/
